//
//  ActivesViewController.swift
//  COWATER
//
//  Created by Manh Blue on 10/8/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

class ActivesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: Delegate?
    var dataList: [FakeData.Active]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
    }
    
    func configTableView() {
        tableView.rowHeight = 72
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        
        if let delegate = delegate as? ReportViewController {
            dataList = delegate.getActiveData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("ActiveViewCell", owner: self, options: nil)?.first as! ActiveViewCell
        cell.activeLabel.text = dataList![indexPath.row].title
        cell.countLabel.text = String(format: "(%d)", dataList!.count)
        
        cell.backgroundColor = (indexPath.row % 2 == 0) ? .white : .myGray
        
        return cell
    }
    
    var objectToSend: [FakeData.Training]?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = TrainingInfoController()
        vc.delegate = self
        
        let ans = dataList?[indexPath.row]
        objectToSend = ans?.trainingList
        
        let titleStr = ans?.title
        let index = titleStr!.index(titleStr!.startIndex, offsetBy: 40)
        navigationItem.title = String(titleStr![..<index]) + "..."
        navigationController?.pushViewController(vc)
    }
}

extension ActivesViewController: Delegate {
    
    func getTrainingData() -> [FakeData.Training]? {
        return objectToSend
    }
}
