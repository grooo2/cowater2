//
//  ActiveViewCell.swift
//  COWATER
//
//  Created by Manh Blue on 10/8/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

class ActiveViewCell: UITableViewCell {
    
    @IBOutlet weak var activeLabel: UILabel! {
        didSet {
            activeLabel.numberOfLines = 2
        }
    }
    
    @IBOutlet weak var countLabel: UILabel!
    
}
