//
//  AppDelegate.swift
//  COWATER
//
//  Created by baonv on 10/7/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configNavigationBar()
        setRoot(LoginViewController())
        
        return true
    }

    private func setRoot(_ vc: UIViewController) {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }

    func showMainScreenAsRoot() {
        let vc = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "QuarterNav")
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }
    
    private func configNavigationBar() {
        UINavigationBar.appearance().height = 30
        //UINavigationBar.appearance().tintColor = UIColor.init(named: "232F5D")
    }
}

