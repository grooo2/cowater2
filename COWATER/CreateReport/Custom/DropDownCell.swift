//
//  DropDownCell.swift
//  COWATER
//
//  Created by Manh Blue on 10/9/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

class DropDownCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var imageCell: UIImageView!
    
}
