//
//  DatePickerView.swift
//  COWATER
//
//  Created by Manh Blue on 10/9/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

class DatePickerView: UIViewController {
    
    
    @IBOutlet weak var datePicker: UIDatePicker!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func saveDate(_ sender: Any) {
        print(datePicker.date)
        dismiss(animated: true, completion: nil)
    }
    
}
