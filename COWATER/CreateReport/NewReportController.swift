//
//  NewReportController.swift
//  COWATER
//
//  Created by Manh Blue on 10/9/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

class NewReportController: UIViewController {
    let items = ["Green", "Red", "Yellow"]

    @IBOutlet var views: [UIView]?
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var dropDownTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configSaveButton()
        configInputView()
        setHiddenDropDown()
    }
    
    func setHiddenDropDown() {
        dropDownTable.isHidden = true
    }
    
    func configSaveButton() {
        saveButton.layer.cornerRadius = 22.5
    }
    
    func configInputView() {
        navigationItem.title = "NEW REPORT"
        let config = {(_ views: [UIView]) in
            for view in views {
                view.layer.borderWidth = 1
                view.layer.borderColor = UIColor.myGrey.cgColor
            }
        }
        
        config(views!)
    }
    
    @IBAction func calendarButton1(_ sender: Any) {
        showDatePickerView()
    }
    
    @IBAction func calendarButton2(_ sender: Any) {
        showDatePickerView()
    }
    
    func showDatePickerView() {
        present(DatePickerView(), animated: true)
    }
    
/*
    @IBOutlet weak var buttonToClick: UIButton!
    
    
    @IBAction func clickDropDown(_ sender: Any) {
        
    }
 */
}
/*
extension NewReportController: UITableViewDelegate, UITableViewDataSource {
    func configDataSoucre() {
        dropDownTable.dataSource = self
        dropDownTable.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = DropDownCell()
        cell.title.text = items[indexPath.row]
        cell.imageCell.image = UIImage(named: "done")
        
        return cell
    }
    
    
}
 */
