//
//  ReportViewController.swift
//  COWATER
//
//  Created by baonv on 10/7/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit
import SideMenu

class ReportViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var menu: SideMenuNavigationController?
    
    var fakeData: [FakeData.Report]?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configBackButton()
        configTableView()
        configNavigationBar()
        initSideMenu()
        initFakeData()
    }
    
    private func initFakeData() {
        fakeData = [FakeData.Report](repeating: .init(), count: Int.random(in: 1...10))
    }
    
    private func configBackButton() {
        navigationItem.hidesBackButton = true
    }
    
    private func initSideMenu() {
        menu = SideMenuNavigationController(rootViewController: MenuController())
        menu?.leftSide = true
        menu?.setNavigationBarHidden(true, animated: true)
        
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        
    }
    
    private func configNavigationBar() {
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.title = "Báo cáo"
    }
    
    private func configTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 72
        navigationController?.navigationBar.topItem?.title = "NEW REPORT"
    }

    @IBAction func didTapMenu(_ sender: Any) {
        present(menu!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fakeData!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("ReportViewCell", owner: self, options: nil)?.first as! ReportViewCell
        cell.title.text = fakeData?[indexPath.row].title
        
        return cell
    }
    
    var objectToSend: [FakeData.Active]?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ActivesViewController()
        vc.delegate = self
        
        let ans = fakeData?[indexPath.row]
        self.navigationItem.backBarButtonItem?.title = ans?.title
        objectToSend = ans?.activeData
        navigationController?.pushViewController(vc)
    }
}

extension ReportViewController: Delegate {
    
    func getActiveData() -> [FakeData.Active]? {
        return objectToSend
    }
}
