//
//  MenuController.swift
//  COWATER
//
//  Created by Manh Blue on 10/7/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit


class MenuController: UITableViewController {
    
    var items = [
        MenuItem(image: "ic_report", title: "Report"),
        MenuItem(image: "ic_guide", title: "Guide"),
        MenuItem(image: "ic_intro-1", title: "Introduction"),
        MenuItem(image: "ic_settings", title: "Setting"),
        MenuItem(image: "ic_logout", title: "Logout")
    ]
    
    override func viewDidLoad() {
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.myBoldPurple
        tableView.headerView(forSection: 1)?.backgroundColor = UIColor.myBoldPurple
        tableView.rowHeight = 84
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("MyTableViewCell", owner: self, options: nil)?.first as! MyTableViewCell
        
        let item = items[indexPath.row]
        cell.icon.image = UIImage(named: item.image)
        cell.title.text = item.title
        cell.backgroundColor = UIColor.myBoldPurple
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.15) {
            //tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.myPurple
        }
    }
    
    /***************************/
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return UserDefaults.standard.value(forKey: "accountName") as! String
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 48
    }
}


