//
//  TrainingCell.swift
//  COWATER
//
//  Created by Manh Blue on 10/8/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

class TrainingCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var dateTime: UILabel!
    
}
