//
//  TrainingInfoController.swift
//  COWATER
//
//  Created by Manh Blue on 10/8/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

class TrainingInfoController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var myTableView: UITableView!
    
    @IBOutlet weak var numberTraining: UILabel!
    
    var delegate: Delegate?
    
    var trainingList: [FakeData.Training]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.rowHeight = 72
        myTableView.dataSource = self
        getTrainingList()
        configView()
    }
    
    
    private func getTrainingList() {
        if let delegate = delegate as? ActivesViewController {
            trainingList = delegate.getTrainingData()
        }
    }
    
     func configView() {
        
        numberTraining.text = String(format: "(%d)", trainingList?.count ?? 0)
        
        button.layer.cornerRadius = 24
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center

        let attribute1: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 48),
            .foregroundColor: UIColor.white,
            .paragraphStyle: paragraphStyle,
            .baselineOffset: 2
        ]
        
        let attribute2: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 20),
            .foregroundColor: UIColor.white,
            .paragraphStyle: paragraphStyle,
            .baselineOffset: 8
        ]
        
        let attributeIcon = NSAttributedString(string: "+", attributes: attribute1)
        let attributeText = NSAttributedString(string: " ADD NEW", attributes: attribute2)
        let title = NSMutableAttributedString()
        title.append(attributeIcon)
        title.append(attributeText)
        
        button.setAttributedTitle(title, for: .normal)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trainingList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("TrainingCell", owner: self, options: nil)?.first as! TrainingCell
        cell.title.text = trainingList?[indexPath.row].content
        cell.dateTime.text = trainingList?[indexPath.row].dateAndTime
        
        cell.backgroundColor = (indexPath.row % 2 == 0) ? .white : UIColor.myGray
        return cell
    }
    
    @IBAction func addNewButton(_ sender: Any) {
        navigationController?.pushViewController(NewReportController())
    }
    
}
