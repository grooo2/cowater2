//
//  LoginViewController.swift
//  COWATER
//
//  Created by baonv on 10/7/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit
import SwifterSwift

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButonLabel: UIButton!
    
//    name: "Leanne Graham",
//    username: "Bret",
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.text = "Leanne Graham"
        passwordTextField.text = "Bret"
        configTextField()
    }
    
    @IBAction func loginButton(_ sender: Any) {
        if Request.shared.isLoginValid(username: usernameTextField.text, password: passwordTextField.text) {
            let storyboard = UIStoryboard(name: View.main.rawValue, bundle: nil)
            let navigationController = storyboard.instantiateViewController(identifier: View.navigation.rawValue)
            navigationController.modalPresentationStyle = .fullScreen
            navigationController.modalTransitionStyle = .crossDissolve
            
            UserDefaults.standard.set(usernameTextField.text, forKey: "accountName")
            present(navigationController, animated: true)
        }
    }
}

extension LoginViewController {
    func configTextField() {
        
        let config = { (view: UIView, title: String, border: Bool) in
            view.layer.cornerRadius = view.height/2
            if border {
                view.borderWidth = 1
                view.borderColor = .white
            }
            if let textField = view as? UITextField {
                textField.attributedPlaceholder = String.attribute(string: title, color: .white, bold: false)
                textField.addPaddingLeft(16)
            }
            if let button = view as? UIButton {
                let attribute = String.attribute(string: "LOGIN", color: .white, bold: true)
                button.setAttributedTitle(attribute, for: .normal)
            }
        }
        
        config(loginButonLabel, "LOGIN", false)
        config(usernameTextField, "Username", true)
        config(passwordTextField, "Password", true)
        passwordTextField.isSecureTextEntry = true
    }
}
