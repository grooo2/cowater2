//
//  UIColor+Extension.swift
//  COWATER
//
//  Created by Manh Blue on 10/8/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

extension UIColor {
    static var myBoldPurple =  UIColor(named: "232F5D")!
    static var myPurple = UIColor(named: "424E7D")!
    static var myGray = UIColor(named: "F7F8F8")!
    static var myGrey = UIColor(named: "444444")!
}

