//
//  Defines.swift
//  COWATER
//
//  Created by baonv on 10/7/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import Foundation


enum View: String {
    case navigation
    case main = "Main"
}

struct MenuItem {
    var image: String
    var title: String
}
