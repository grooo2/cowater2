//
//  UIImage+Extension.swift
//  COWATER
//
//  Created by baonv on 10/9/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

extension UIImage {
    static let calendar = UIImage(named: "calendar")
    static let doneImage = UIImage(named: "done")
}
