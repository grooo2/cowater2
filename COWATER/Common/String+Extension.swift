//
//  String+Extension.swift
//  COWATER
//
//  Created by baonv on 10/7/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

extension String {
    static func attribute(string: String?, color: UIColor, bold: Bool) -> NSAttributedString {
        let font = bold ? UIFont.boldSystemFont(ofSize: 16) : UIFont.systemFont(ofSize: 16)
        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.alignment = .left
        paragraphStyle.firstLineHeadIndent = 0

        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.white,
            .paragraphStyle: paragraphStyle
        ]
        
        let attribute = NSAttributedString(string: string ?? "", attributes: attributes)
        return attribute
    }
}
