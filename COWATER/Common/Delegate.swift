//
//  Delegate.swift
//  COWATER
//
//  Created by Manh Blue on 10/8/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import Foundation

protocol Delegate {
    func getActiveData() -> [FakeData.Active]?
    func getTrainingData() -> [FakeData.Training]?
}

extension Delegate {
    func getActiveData() -> [FakeData.Active]? {
        return nil
    }
    func getTrainingData() -> [FakeData.Training]? {
        return nil
    }
}
