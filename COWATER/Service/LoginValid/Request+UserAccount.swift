//
//  RequestUserAccount.swift
//  COWATER
//
//  Created by baonv on 10/7/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit
import Alamofire

class Request {
    static var shared = Request()
    private let baseURL = "https://jsonplaceholder.typicode.com"

    func isLoginValid(username: String?, password: String?) -> Bool {
        
        AF.request(baseURL + "/users").responseJSON {  response in
            guard let data = response.data else {
                return
            }

            guard let userDict = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
                return
            }

            for user in userDict {
                let usernameFetch = user[UserKey.username.rawValue] as? String
                let passwordFetch = user[UserKey.password.rawValue] as? String
                if username == usernameFetch && password == passwordFetch {
                    print("Login Valid")
                    return
                }
            }
        }
        
        return true
    }
}
