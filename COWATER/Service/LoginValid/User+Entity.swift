//
//  User+Entity.swift
//  COWATER
//
//  Created by baonv on 10/7/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

enum UserKey: String {
    case username = "name"
    case password = "username"
}

struct User: Decodable {
    var username: String?
    var password: String?
}
