//
//  Data+Fake.swift
//  COWATER
//
//  Created by Manh Blue on 10/8/20.
//  Copyright © 2020 baonv. All rights reserved.
//

import UIKit

struct FakeData {
    class Training {
        var content: String = "Training woman in Son La day 1"
        var dateAndTime: String = "05-08-2020 10h:20:00"
    }
    
    class Active {
        var title = "HĐ 1.4.1.1 nâng cao nhận thức của phụ nữ các dân tộc vùng cao Lào Cai"
        var trainingList: [Training] = [Training](repeating: Training(), count: Int.random(in: 1...50))
    }

    class Report {
        var title = "FY 19/20 - T: 3, 4, 5"
        var activeData: [Active] = [Active](repeating: Active(), count: Int.random(in: 1...50))
    }
    
}
